# Gitlab pages
Welcome to an example page! 
Below you will find a detailed instruction on how to set-up your own gitlab website.
For a better view, please go to the following website: [https://core-services.pages.uni.lu/pages-jekyll-lcsb-template/](https://core-services.pages.uni.lu/pages-jekyll-lcsb-template/)

Sources for this page are [available in Gitlab](https://git-r3lab.uni.lu/core-services/pages-jekyll-lcsb-template).
Additionally, you can also navigate to the [repository for theme](https://git-r3lab.uni.lu/core-services/jekyll-theme-lcsb-default).

# Setting up your web page

## The process
0. Make sure that you have access to [gitlab](https://git-r3lab.uni.lu/). If you ever cloned a repository or set-up a new one there, then you should be fine.
1. [Create an empty repository in gitlab](https://git-r3lab.uni.lu/projects/new). Please bear in mind, that the both names of the namespace and the project influence 
    final address of the page - it will follow the `https://<namespace>.pages.uni.lu/<project_name>` convention.
2. Clone this very repository: `git clone ssh://git@git-r3lab-server.uni.lu:8022/core-services/pages-jekyll-lcsb-template.git`.
3. Once cloned, remove the _remote_ (so that every time you push to your repository, you don't push to this very repository):
`cd project_name; git remote rm origin`, and add _remote_ to your new repository (you can find the correct remote address and the appropriate command in Gitlab, in your new repository page) **TODO: add screenshot**.
4. Modify site's settings (`_config.yml`) to match your needs. Refer to the next section for help.
5. Modify the index page. Modify (or delete) help and about pages. Add your own content. 
6. Commit and push to the repository.
7. If you want to have your page publicly available, make a ticket to SIU.
8. Your page is published! Go to `https://<namespace>.pages.uni.lu/name-of-repository` in your favourite browser, or the URL you specified in the SIU ticket.
9. (optional) In gitlab, go to **Settings** (under left-hand menu) > **General** > **Advanced** (hit `Expand` button) > **Remove fork relationship** (red button), then follow the instructions from the pop-up.

## What should you change in settings file?
Mandatory:
 * `baseurl` - this is very important. Set it to the name of the project (i.e. repository name). Here it's `pages-jekyll-lcsb-template`
 * `url` - this is equally important. Please set it to `https://gitlab-namespace-name.pages.uni.lu`. For example - if your project is inside `core-services` namespace, it should be: `url: "https://core-services.pages.uni.lu"`. For `minerva`, it would be: `url: "https://minerva.pages.uni.lu"`.
 
Optional:
 * `title` field
 * `e-mail` field
 * `description` field
 * `date` field
 * `banner` field - if you want to have your own banner (the text next to _uni.lu_ logo), please send us an email (`lcsb-sysadmins (at) uni.lu`).
 
## Testing the web page locally
You can test your website locally (on your machine). 

* First, make sure that you have Ruby installed. If not - please [install it](https://www.ruby-lang.org/en/downloads/).
* Then, install _bundler_ - `gem install bundler`. 
* Next, initialize the site: `bundle install`.
* Finally, run the site: `bundle exec jekyll serve`.


## Common problems
### *The website looks broken! There are no images, no colors etc.*
You probably didn't configure `baseurl` parameter in the settings. Please take a look on `_settings.yml` file.

### *The links in the menu are not working (they point to "404: Not found").*
You probably didn't add `permalink` attribute. Or the post has `published: false` or `draft: true` set. Please take a look on the post file.

### Other problems?
Please send us an email! (`lcsb-sysadmins (at) uni.lu`)