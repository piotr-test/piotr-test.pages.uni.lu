---
layout: page
title: About
permalink: /about/
order: 2
---

This is the LCSB Jekyll theme. You can find out more info about customizing your Jekyll theme, as well as basic Jekyll usage documentation at [jekyllrb.com](https://jekyllrb.com/)

You can find the source code for this them at Gitlab:
[core-services/jekyll-theme-lcsb-default](https://git-r3lab.uni.lu/core-services/jekyll-theme-lcsb-default).

We have successfully helped in publishing the following websites:
 * [Page of theme](https://core-services.pages.uni.lu/jekyll-theme-lcsb-default/)
 * [This very page](https://core-services.pages.uni.lu/pages-jekyll-lcsb-template/)
 * [Minerva doc's](https://minerva.pages.uni.lu/doc/)
 * [PDmap blog](https://pdmap.pages.uni.lu/blog/)
 * [IMP](https://imp.pages.uni.lu/web/)